//package com.mwj.auction.config;
//
///**
// * @author 马文杰
// * @version 1.0
// */
//
//import com.mwj.rabbitclient.service.impl.CommodityOrderServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.connection.Message;
//import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
//import org.springframework.data.redis.listener.RedisMessageListenerContainer;
//import org.springframework.stereotype.Component;
//
///**
// * Redis事件监听器
// */
//@Component
//public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
//    @Autowired
//    CommodityOrderServiceImpl commodityOrderService;
//    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
//        super(listenerContainer);
//    }
//
//
//    //处理:之后的值，去掉后面"
//    public String afterVal(String val){
//        String value = val.substring(val.indexOf(":") + 1);
//        return value.substring(0, value.length() - 1);
//    }
//    //处理:前面的值，去掉前面"
//    public String topVal(String val){
//        String key = val.substring(0, val.indexOf(":"));
//        System.out.println(key.substring(1));
//        return key.substring(1);
//    }
//
//
//    /**
//     * 重写omMessage方法，当Redis中的Key过期时会执行该方法
//     */
//    @Override
//    public void onMessage(Message message, byte[] pattern) {
//        // 过期的key
//        String expireKey = message.toString();
//        System.out.println("Redis过期key："+expireKey);
//        if (topVal(expireKey).equals("OrderValidityTime")){
//            String values = afterVal(expireKey);
//            System.out.println(values);
//            //订单超时直接修改订单信息
//            commodityOrderService.updateOrderNO(values);
//        }
//
//    }
//}
