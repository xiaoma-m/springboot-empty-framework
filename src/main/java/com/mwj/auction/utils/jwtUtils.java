package com.mwj.auction.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author 马文杰
 * @version 1.0
 */
public class jwtUtils {

    //加密token
    public static String getToken(String userid) {
        //设置第一段头部
        JWTCreator.Builder builder = JWT.create();
        //设置加密属性
        builder.withClaim("id", userid);
        //创建时间加上有效期
        LocalDateTime time = LocalDateTime.now();

        //   time = time.plusMinutes(1);    //设置jwt过期时间10分钟
        time = time.plusDays(30);       //设置过期时间30天
        Instant instant = time.atZone(ZoneId.systemDefault()).toInstant();
        builder.withExpiresAt(Date.from(instant));
        //设置加密方式
        String token = builder.sign(Algorithm.HMAC256("*173jdD2s@6dg"));
        return token;
    }


    //解密token
    public static String parseToken(String token) {
        try {
            //设置解密方式
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256("*173jdD2s@6dg")).build();
            DecodedJWT decodedJWT = verifier.verify(token);
            //解密之后把参数直接返回
            return decodedJWT.getClaim("id").asString();
        } catch (Exception e) {
            return null;
        }
    }
}
