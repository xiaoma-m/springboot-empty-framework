package com.mwj.auction.utils;

/**
 * @author 马文杰
 * @version 1.0
 */
import java.util.Random;

/**
 * 随机生成验证码工具类
 */
public class ValCodeUtils {
    /**
     * 随机生成验证码
     * @param length 长度为4位或者6位
     * @return
     */
    public static String generateValidateCode(int length){
        Integer code =null;
        if(length == 4){
            code = new Random().nextInt(9999);//生成随机数，最大为9999
            if(code < 1000){
                code = code + 1000;//保证随机数为4位数字
            }
        }else if(length == 6){
            code = new Random().nextInt(999999);//生成随机数，最大为999999
            if(code < 100000){
                code = code + 100000;//保证随机数为6位数字
            }
        }else{
            throw new RuntimeException("只能生成4位或6位数字验证码");
        }
        return String.valueOf(code);
    }

    /**
     * 随机生成指定长度字符串验证码
     * @param length 长度
     * @return
     */
    public static String generateValidateCode4String(int length){
        Random rdm = new Random();
        String hash1 = Integer.toHexString(rdm.nextInt());
        String capstr = hash1.substring(0, length);
        return capstr;
    }

    /**
     * 随机生成8位数Id
     * @return
     */
    public static String generateRandomId() {
        Random random = new Random();
        // 生成后7位数字
        StringBuilder numericPart = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            numericPart.append(random.nextInt(10));
        }
        return numericPart.toString();
    }
}

