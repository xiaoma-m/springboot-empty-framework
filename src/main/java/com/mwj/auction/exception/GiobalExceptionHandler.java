package com.mwj.auction.exception;

import com.mwj.auction.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author 马文杰
 * @version 1.0
 */

@RestControllerAdvice       //处理@Component中的异常的
@Slf4j
public class GiobalExceptionHandler{
    @ResponseBody
    @ExceptionHandler(SystemException.class)    //出现异常会调用该方法来处理异常
    public Result systemExceptionHandler(SystemException e){
        return Result.error(e.getCode(),e.getMsg());
    }
}
