package com.mwj.auction.exception;


/**
 * @author 马文杰
 * @version 1.0
 */
public class SystemException extends RuntimeException{
    int code;
    String msg;

    public int getCode(){
        return code;
    }

    public String getMsg(){
        return msg;
    }

    public SystemException(){
        this.code = 202;
        this.msg = "登录失效，请重新登录";
    }
}
