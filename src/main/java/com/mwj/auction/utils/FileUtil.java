package com.mwj.auction.utils;

import net.coobird.thumbnailator.Thumbnails;
import ws.schild.jave.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil {

    /**
     * 压缩图片文件大小
     *
     * @param inputStream  输入图片流
     * @param outputStream 输出图片流
     * @param quality      压缩质量 (0.0 到 1.0，1.0表示最高质量)
     * @throws IOException
     */
    public static void compressImage(InputStream inputStream, OutputStream outputStream, float quality) throws IOException {
        // 使用Thumbnailator进行压缩
        Thumbnails.of(inputStream)
                .scale(1.0) // 保持原始尺寸
                .outputQuality(quality)
                .toOutputStream(outputStream);
    }


    /**
     * 测试本地视频压缩这是地址  /Users/mawenjie/Downloads/kkst.mp4
     */
    public static void main(String[] args) {
        File source = new File("/Users/mawenjie/Movies/kkts.mp4");
        File target = compressionVideo(source, "kkst_compressed.mp4");
        System.out.println(target.getAbsolutePath());
    }

    /**
     * 压缩视频文件
     * @param source 原始视频文件
     * @param outputFileName 压缩后的视频文件名
     * @return 压缩后的视频文件
     */
    public static File compressionVideo(File source, String outputFileName) {
        if (source == null) {
            return null;
        }
        String outputPath = source.getParent() + File.separator + outputFileName;
        File target = new File(outputPath);

        try {
            MultimediaObject multimediaObject = new MultimediaObject(source);
            AudioInfo audioInfo = multimediaObject.getInfo().getAudio();
            VideoInfo videoInfo = multimediaObject.getInfo().getVideo();

            // 配置音频属性
            AudioAttributes audio = new AudioAttributes();
            audio.setCodec("aac");
            audio.setBitRate(audioInfo.getBitRate() > 128000 ? 128000 : audioInfo.getBitRate());
            audio.setChannels(audioInfo.getChannels());
            audio.setSamplingRate(audioInfo.getSamplingRate() > 44100 ? 44100 : audioInfo.getSamplingRate());

            // 配置视频属性
            VideoAttributes video = new VideoAttributes();
            video.setCodec("h264");
            video.setBitRate(videoInfo.getBitRate() > 500000 ? 500000 : videoInfo.getBitRate());
            video.setFrameRate(videoInfo.getFrameRate() > 20 ? 20 : (int) videoInfo.getFrameRate());

            // 限制视频宽度
            int maxWidth = 1280;
            if (videoInfo.getSize().getWidth() > maxWidth) {
                float ratio = (float) videoInfo.getSize().getWidth() / maxWidth;
                video.setSize(new VideoSize(maxWidth, (int) (videoInfo.getSize().getHeight() / ratio)));
            }

            // 设置编码属性
            EncodingAttributes attributes = new EncodingAttributes();
            attributes.setFormat("mp4");
            attributes.setAudioAttributes(audio);
            attributes.setVideoAttributes(video);

            // 执行压缩
            Encoder encoder = new Encoder();
            encoder.encode(multimediaObject, target, attributes);
            return target;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return source; // 如果压缩失败，返回原始文件
    }

    /**
     * 压缩音频文件
     */
    public static File compressionAudio(File source, String outputFileName) {
        if (source == null) {
            return null;
        }
        String outputPath = source.getParent() + File.separator + outputFileName;
        File target = new File(outputPath);

        try {
            MultimediaObject multimediaObject = new MultimediaObject(source);
            AudioInfo audioInfo = multimediaObject.getInfo().getAudio();

            // 配置音频属性
            AudioAttributes audio = new AudioAttributes();
            audio.setCodec("aac");
            audio.setBitRate(audioInfo.getBitRate() > 128000 ? 128000 : audioInfo.getBitRate());
            audio.setChannels(audioInfo.getChannels());
            audio.setSamplingRate(audioInfo.getSamplingRate() > 44100 ? 44100 : audioInfo.getSamplingRate());

            // 设置编码属性
            EncodingAttributes attributes = new EncodingAttributes();
            attributes.setFormat("mp3");
            attributes.setAudioAttributes(audio);

            // 执行压缩
            Encoder encoder = new Encoder();
            encoder.encode(multimediaObject, target, attributes);
            return target;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return source; // 如果压缩失败，返回原始文件
        }
}
