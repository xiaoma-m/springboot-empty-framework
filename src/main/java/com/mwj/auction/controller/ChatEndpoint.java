//package com.mwj.auction.controller;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.CrossOrigin;
//
//import javax.websocket.OnClose;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.Session;
//import javax.websocket.server.PathParam;
//import javax.websocket.server.ServerEndpoint;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.*;
//
///**
// * @author 马文杰
// * @version 1.0
// */
//@ServerEndpoint("/chat/{MerchantsChat}")
//@CrossOrigin("*")
//@Component
//public class ChatEndpoint {
//    private static final ObjectMapper objectMapper = new ObjectMapper(); // 用于解析 JSON
//
//
//
//    //需要某一个内需要这样静态注入
//    //   private static UserMsgMapper userMsgMapper;
//
//    //    @Autowired
//    //    public void setFriendMapper(FriendMapper friendMapper) {
//    //        ChatEndpoint.friendMapper = friendMapper;  // 静态注入
//    //    }
//
//    private static final Map<String,Session> onlineUsers = new ConcurrentHashMap<>();
//
//    /**
//     * 建立webSocke连接后,被调用
//     * @param session
//     */
//    @OnOpen
//    public void onOpen(Session session, @PathParam("MerchantsChat") String username) {
////        boolean b = csService.checkTvStatus();
//        //1.将session存入
//        //2.广播消息,需要将登录的所有用户推送给所有用户
//        //3.将消息存入数据库
//        System.out.println("对象------"+username +"连接成功");
//        //返回给自己，连接成功
//        try {
////            session.getBasicRemote().sendText("连接成功");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //取出username第一个字符，判断是否为Q
//        System.out.println(username.charAt(0));
////        if(){
////
////        }
//        onlineUsers.put(username, session);
//    }
//
//    //广播消息
//    public void broadcast(String MerchantsChat) {
//        //如何给接受者发送消息?
//
//        onlineUsers.forEach((username, session) -> {
//            try {
////                session.getBasicRemote().sendText(MerchantsChat);
//                System.out.println("发送消息成功");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//    }
//
//    //给所有人发送消息，更新商品信息
//    public void sendAll(String MerchantsChat) {
//        //如何给接受者发送消息?
//        onlineUsers.forEach((username, session) -> {
//            try {
//                session.getBasicRemote().sendText(MerchantsChat);
//                System.out.println("发送消息成功");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//    }
//
//    //接收到消息
//    @OnMessage
//    public void onMessage(String MerchantsChat) {
//        System.out.println("收到消息：" + MerchantsChat);
//    }
//
///**
//     * 连接关闭后,被调用
//     * @param session
//     */
//    @OnClose
//    public void onClose(Session session) {
//        //从集合中移除
//        onlineUsers.remove(session.getId());
////        broadcast("用户" + session.getId() + "离开聊天室");
//        System.out.println("连接关闭");
//    }
//
//    /**
//     * 发送消息给指定用户
//     */
//    public void sendToUser(String receiver, String message) {
//        Session receiverSession = onlineUsers.get(receiver);  // 查找接收者的 Session
//        if (receiverSession != null && receiverSession.isOpen()) {
//            try {
//                receiverSession.getBasicRemote().sendText(message);
//                System.out.println("消息发送成功给：" + receiver);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            System.out.println("用户 " + receiver + " 不在线或连接已关闭");
//        }
//    }
//
//
//}
