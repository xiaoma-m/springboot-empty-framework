package com.mwj.auction.utils;

/**
 * @author 马文杰
 * @version 1.0
 */
public enum HttpCodeEnum {
    SUCCESS(200,"操作成功"),
    ERROE(400,"操作失败"),
    GET_SUCCESS(200,"获取成功"),
    LOGIN_NO(401,"需要登录后才能操作"),
    LIMITS_NO(500,"认证授权失败"),
    LOGIN_SUCCESS(200,"登录成功"),
    SMS_CODE_SUCCESS(200,"发送成功"),
    SMS_CODE_NO(500,"验证码错误"),
    USER_NO_LIMITS(403,"用户权限不足"),
    REQUEIRE_USERNAME(504,"必须填写账号"),
    USER_REALA_NO(504,"请输入正确的身份证和姓名"),
    USER_REALA_OK(200,"实名认证成功"),
    USER_REALA(504,"已实名认证"),
    USER_NICKNAME_NO(504,"请输入1-10个字符"),
    NO_ISSM(400,"未实名"),
    ADDRESS_NO_DEFAULT(400,"未添加默认地址"),
    PAY_PASSWORD_NO1(400,"两次密码不一致"),
    PAY_PASSWORD_NO2(400,"密码必须是6位纯数字"),
    PAY_PASSWORD_NO3(400,"密码输入错误"),
    ACCEPT_ID_NO(400,"请输入正确的接受者id"),
    ACCEPT_ID_OK(200,"赠送成功"),
    PROPS_SUCCESS_OK(200,"购买成功"),
    PROPS_RADISH_NO(400,"萝卜不足"),
    PROPS_RABBIT_NO(400,"兔子不足"),
    PROPS_CRYSTAL_NO(400,"钻石不足"),
    INVITATION_BINDING1(200,"绑定成功"),
    INVITATION_BINDING2(400,"邀请码不存在"),
    INVITATION_BINDING3(400,"已绑定邀请码");

    int code;
    String msg;

    HttpCodeEnum(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }
}
